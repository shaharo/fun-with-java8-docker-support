echo ---------building image ----------
docker build . -f Dockerfile_java_no_support -t java8_no_docker_support
echo ---------creating container --------
OUTPUT="$(docker create -m 1G --cpus 1 --name=java8_no_docker_support_container -it java8_no_docker_support)"
echo container id :  ${OUTPUT}
echo --------starting container---------
docker start java8_no_docker_support_container
echo -------executing----------
docker exec java8_no_docker_support_container /script_in_docker.sh
echo --------stopping conatiner----------
docker stop java8_no_docker_support_container
echo ---------rm container------------
docker rm ${OUTPUT}
echo ---------building image ----------
docker build . -f Dockerfile_java_support -t java8_docker_support
echo ---------creating container --------
OUTPUT="$(docker create -m 1G --cpus 1 --name=java8_docker_support_container -it java8_docker_support)"
echo container id :  ${OUTPUT}
echo --------starting container---------
docker start java8_docker_support_container
echo -------executing----------
docker exec java8_docker_support_container /script_in_docker.sh
echo --------stopping conatiner----------
docker stop java8_docker_support_container
echo ---------rm container------------
docker rm ${OUTPUT}
