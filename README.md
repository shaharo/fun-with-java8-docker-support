# fun-with-java8-docker-support

Just a test project to see how java8u212 docker supported has affected the runtime when running in docker container.

creates two containers from these base images
1. using openjdk:8u181-jdk - which don't have docker support
2. using openjdk:8u212-jdk - which have docker support

then , running starting them while limiting the container to 1 GB of memory and to 1 processor.
when startin the container compiles and run Main.java file , which prints out max memory and available processors.

executing script.sh creates and runs everything while outputting the difference between the two containers.



